package com.pawelrak.utils;

import com.pawelrak.data.model.Comment;
import com.pawelrak.data.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by pawelrak on 25.04.2017.
 */

public class MyDataSource {

    //It's just for the purpose of showcase
    public static ArrayList<Project> dataSource =
            new ArrayList<>();

    static {
        Project p1 = new Project(1, "Project 1");
        p1.setDescription("This is decription 1");
        p1.setCreationDate(new Date());
        p1.setComments(new ArrayList<Comment>());
        Project p2 = new Project(2, "Project 2");
        p2.setDescription("This is decription 2");
        p2.setCreationDate(new Date());
        p2.setInReview(true);
        p2.setComments(new ArrayList<Comment>());
        Project p3 = new Project(3, "Project 3");
        p3.setDescription("This is decription 3");
        p3.setCreationDate(new Date());
        p3.setComments(new ArrayList<Comment>());

        dataSource.addAll(Arrays.asList(p1, p2, p3));
    }
}
