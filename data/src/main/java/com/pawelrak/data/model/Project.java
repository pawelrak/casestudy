package com.pawelrak.data.model;

import java.util.Date;
import java.util.List;

/**
 * Created by pawelrak on 25.04.2017.
 */

public class Project {

    int id;
    String name;
    String description;
    Date creationDate;
    List<Comment> comments;
    boolean inReview;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean isInReview() {
        return inReview;
    }

    public void setInReview(boolean inReview) {
        this.inReview = inReview;
    }
}
