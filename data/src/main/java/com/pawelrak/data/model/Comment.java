package com.pawelrak.data.model;

/**
 * Created by pawelrak on 25.04.2017.
 */

public class Comment {
    String content;

    public Comment(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
