package com.pawelrak.casestudy.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.utils.Utils;
import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.model.Project;
import com.pawelrak.utils.MyDataSource;

import javax.inject.Inject;

/**
 * Created by pawelrak on 25.04.2017.
 */

@ActivityScope
public class AddProjectViewModel extends BaseViewModel {


    @Inject
    public AddProjectViewModel(NavigationBus navigationBus) {
        super(navigationBus);
    }

}
