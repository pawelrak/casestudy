package com.pawelrak.casestudy.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.utils.Utils;
import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.model.Project;
import com.pawelrak.utils.MyDataSource;

import javax.inject.Inject;

/**
 * Created by pawelrak on 25.04.2017.
 */

@ActivityScope
public class ProjectDetailsViewModel extends BaseViewModel {

    public ObservableField<String> projectName;
    public ObservableField<String> projectDescription;
    public ObservableField<String> creationDate;
    public ObservableBoolean commentsVisible;

    @Inject
    public ProjectDetailsViewModel(NavigationBus navigationBus) {
        super(navigationBus);
        projectName = new ObservableField<>();
        projectDescription = new ObservableField<>();
        creationDate = new ObservableField<>();
        commentsVisible = new ObservableBoolean(false);
    }

    public void fetchProjects(int id) {
        for(Project p : MyDataSource.dataSource) {
            if(p.getId() == id) {
                projectName.set(p.getName());
                projectDescription.set(p.getDescription());
                creationDate.set(Utils.df.format(p.getCreationDate()));
                if(!p.getComments().isEmpty())
                    commentsVisible.set(true);
            }
        }
    }

}
