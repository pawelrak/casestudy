package com.pawelrak.casestudy.viewmodel;

import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.util.Log;

import com.pawelrak.casestudy.BR;
import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.R;
import com.pawelrak.casestudy.adapter.ClickHandler;
import com.pawelrak.casestudy.adapter.binder.ItemBinder;
import com.pawelrak.casestudy.adapter.binder.ItemBinderBase;
import com.pawelrak.casestudy.navigation.event.ActivityNavigationEvent;
import com.pawelrak.casestudy.view.AddProjectActivity;
import com.pawelrak.casestudy.view.ProjectDetailsActivity;
import com.pawelrak.data.dagger.ActivityScope;
import com.pawelrak.data.model.Project;
import com.pawelrak.utils.MyDataSource;

import javax.inject.Inject;

/**
 * Created by pawelrak on 25.04.2017.
 */

@ActivityScope
public class MainViewModel extends BaseViewModel {

    public ObservableArrayList<Project> projects;

    @Inject
    public MainViewModel(NavigationBus navigationBus) {
        super(navigationBus);
        projects = new ObservableArrayList<>();
        projects.addAll(MyDataSource.dataSource);
    }

    public ItemBinder<Project> itemViewBinder() {
        return new ItemBinderBase<>(BR.project, R.layout.project_list_item);
    }

    public ClickHandler<Project> getClickHandler() {
        return (view, item) -> {
            Log.d("Pawel", item.getName() + " clicked");
            proceedToProjectDetails(item);
        };
    }


    public void addProject() {
        Log.d("Pawel", "add project");
        navigationBus.send(new ActivityNavigationEvent(false, AddProjectActivity.class));
    }

    void proceedToProjectDetails(Project project) {
        Bundle bundle = new Bundle();
        bundle.putInt("projectID", project.getId());
        navigationBus.send(new ActivityNavigationEvent(false, ProjectDetailsActivity.class, bundle));
    }

}
