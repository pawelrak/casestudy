package com.pawelrak.casestudy.viewmodel;

import android.databinding.BaseObservable;

import com.pawelrak.casestudy.NavigationBus;

/**
 * Created by pawelrak on 25.04.2017.
 */

public class BaseViewModel extends BaseObservable {

    protected NavigationBus navigationBus;

    public BaseViewModel(NavigationBus navigationBus) {
        this.navigationBus = navigationBus;
    }

}
