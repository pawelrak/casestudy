package com.pawelrak.casestudy;

import com.pawelrak.casestudy.navigation.event.NavigationEvent;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by pawelrak on 25.04.2017.
 */

@Singleton
public class NavigationBus {

    private final Subject<NavigationEvent, NavigationEvent> bus = new SerializedSubject<>(PublishSubject.create());

    @Inject
    public NavigationBus() {

    }

    public void send(NavigationEvent event) {
        bus.onNext(event);
    }

    public Observable<NavigationEvent> toObservable() {
        return bus;
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }
}
