package com.pawelrak.casestudy.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;

import com.pawelrak.casestudy.R;
import com.pawelrak.casestudy.databinding.ActivityProjectDetailsBinding;
import com.pawelrak.casestudy.viewmodel.ProjectDetailsViewModel;

import javax.inject.Inject;

public class ProjectDetailsActivity extends BaseActivity {

    @Inject
    ProjectDetailsViewModel viewModel;

    ActivityProjectDetailsBinding binding;

    int passedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_project_details);
        binding.setViewModel(viewModel);
        handleBundle();
    }

    private void handleBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("projectID")) {
            passedId = bundle.getInt("projectID");
            viewModel.fetchProjects(passedId);
        }
        Log.d("Pawel", "Project ID: " + passedId);
    }

    @Override
    protected void injectMembers() {
        getActivityComponent().inject(this);
    }

}
