package com.pawelrak.casestudy.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pawelrak.casestudy.CaseStudyApplication;
import com.pawelrak.casestudy.dagger.component.ActivityComponent;
import com.pawelrak.casestudy.dagger.component.ApplicationComponent;

/**
 * Created by pawelrak on 25.04.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    protected void injectMembers() {
    }

    protected void initializeOnViewModelPropertyChangeListener() {
    }

    protected void addOnPropertyChangeListener() {
    }

    protected void removeOnPropertyChangeListener() {
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        injectMembers();
        initializeOnViewModelPropertyChangeListener();
        addOnPropertyChangeListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeOnPropertyChangeListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activityComponent == null) {
            initializeInjector();
        }
    }

    public void initializeInjector() {
        activityComponent = ActivityComponent.Initializer.init(getApplicationComponent());
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null)
            initializeInjector();
        return activityComponent;

    }

    public ApplicationComponent getApplicationComponent() {
        return ((CaseStudyApplication) getApplication()).getApplicationComponent();
    }

}