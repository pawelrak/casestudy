package com.pawelrak.casestudy.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.pawelrak.casestudy.R;
import com.pawelrak.casestudy.databinding.ActivityMainBinding;
import com.pawelrak.casestudy.viewmodel.MainViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    MainViewModel viewModel;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);
        setupList(binding.list);
    }

    private void setupList(RecyclerView list) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        list.setLayoutManager(llm);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list.getContext(),
                llm.getOrientation());
        list.addItemDecoration(dividerItemDecoration);
    }

    @Override
    protected void injectMembers() {
        getActivityComponent().inject(this);
    }
}
