package com.pawelrak.casestudy.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.pawelrak.casestudy.R;
import com.pawelrak.casestudy.databinding.ActivityAddProjectBinding;
import com.pawelrak.casestudy.viewmodel.AddProjectViewModel;

import javax.inject.Inject;

public class AddProjectActivity extends BaseActivity {

    @Inject
    AddProjectViewModel viewModel;

    ActivityAddProjectBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_project);
        binding.setViewModel(viewModel);
    }

    @Override
    protected void injectMembers() {
        getActivityComponent().inject(this);
    }
}
