package com.pawelrak.casestudy;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.pawelrak.casestudy.dagger.component.ApplicationComponent;
import com.pawelrak.casestudy.navigation.Navigator;

import timber.log.Timber;

/**
 * Created by pawelrak on 25.04.2017.
 */

public class CaseStudyApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private ApplicationComponent applicationComponent;
    private Navigator navigator;

    @Override
    public void onCreate() {
        super.onCreate();

        this.initializeInjector();
        initNavigator();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }

    private void initNavigator() {
        navigator = applicationComponent.navigator();
        registerActivityLifecycleCallbacks(this);
    }

    public void initializeInjector() {
        this.applicationComponent = ApplicationComponent.Initializer.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null)
            initializeInjector();
        return this.applicationComponent;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        navigator.setCurrentActivity(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
