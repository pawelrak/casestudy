package com.pawelrak.casestudy.navigation.handler;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;

import com.pawelrak.casestudy.navigation.event.ActivityNavigationEvent;
import com.pawelrak.casestudy.navigation.event.NavigationEvent;

public class ActivityNavigationHandler
        implements NavigationHandler<Activity, ActivityNavigationEvent> {

    @Override
    public boolean canHandle(Activity activity, NavigationEvent navigationEvent) {
        return navigationEvent instanceof ActivityNavigationEvent;
    }

    @Override
    public void handle(Activity activity, ActivityNavigationEvent event) {
        Intent intent = new Intent(activity, event.getActivityToStart());

        // add extras if needed
        if (event.getBundle() != null) {
            intent.putExtras(event.getBundle());
        }

        // handle shared view elements in post Lollipop
        ActivityOptionsCompat options;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP
                && event.getSharedElements() != null) {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                    event.getSharedElements());
            activity.startActivity(intent, options.toBundle());
        } else {
            // start new activity
            activity.startActivity(intent);
        }

        // kill old activity if commanded
        if (event.shouldFinishCaller()) {
            activity.finish();
        }
    }
}