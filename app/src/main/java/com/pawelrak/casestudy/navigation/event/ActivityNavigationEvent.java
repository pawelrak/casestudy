package com.pawelrak.casestudy.navigation.event;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.view.View;

public class ActivityNavigationEvent implements NavigationEvent {

    private Class activityToStart;
    private Bundle bundle;
    private boolean finishCaller;
    private Pair<View, String>[] sharedElements;

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
    }

    public ActivityNavigationEvent(boolean finishCaller, Class activityToStart, Bundle bundle) {
        this.finishCaller = finishCaller;
        this.activityToStart = activityToStart;
        this.bundle = bundle;
    }

    public Class getActivityToStart() {
        return activityToStart;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public Pair<View, String>[] getSharedElements() {
        return sharedElements;
    }

    public boolean shouldFinishCaller() {
        return finishCaller;
    }
}