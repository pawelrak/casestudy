package com.pawelrak.casestudy.navigation;

import android.app.Activity;

import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.navigation.event.NavigationEvent;
import com.pawelrak.casestudy.navigation.handler.NavigationHandler;
import com.pawelrak.casestudy.view.BaseActivity;

import java.util.List;

public class Navigator {

    private final NavigationBus navigationBus;
    private final List<NavigationHandler> navigationHandlers;
    private Activity currentActivity;

    public Navigator(NavigationBus navigationBus, List<NavigationHandler> navigationHandlers) {
        this.navigationBus = navigationBus;
        this.navigationHandlers = navigationHandlers;
        listen();
    }

    public void destroy(Activity activity) {
        if (currentActivity == activity) {
            currentActivity = null;
        }
    }

    public BaseActivity getCurrentActivity() {
        return (BaseActivity) currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    private void handleEvent(NavigationEvent event) {
        for (NavigationHandler navigationHandler : navigationHandlers) {
            if (navigationHandler.canHandle(currentActivity, event)) {
                navigationHandler.handle(currentActivity, event);
                break;
            }
        }
    }

    private void listen() {
        navigationBus.toObservable().subscribe(this::handleEvent);
    }
}