package com.pawelrak.casestudy.navigation.handler;

import android.app.Activity;

import com.pawelrak.casestudy.navigation.event.NavigationEvent;

/**
 * Created by pawelrak on 25.04.2017.
 */

public interface NavigationHandler<A extends Activity, T extends NavigationEvent> {

    boolean canHandle(Activity activity, NavigationEvent navigationEvent);

    void handle(A activity, T event);
}

