package com.pawelrak.casestudy.adapter;

import android.view.View;

public interface LongClickHandler<T> {
    void onLongClick(View view, T item);
}
