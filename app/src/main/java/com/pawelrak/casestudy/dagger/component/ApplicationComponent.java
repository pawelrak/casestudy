package com.pawelrak.casestudy.dagger.component;

import android.content.Context;

import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.dagger.module.ApplicationModule;
import com.pawelrak.casestudy.navigation.Navigator;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pawelrak on 25.04.2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    Context context();
    Navigator navigator();
    NavigationBus rxBus();

    class Initializer {
        private Initializer() {
//            no instance
        }

        public static ApplicationComponent init(Context context) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(context))
                    .build();
        }
    }
}