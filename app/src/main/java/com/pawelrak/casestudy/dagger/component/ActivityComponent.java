package com.pawelrak.casestudy.dagger.component;

import com.pawelrak.casestudy.view.AddProjectActivity;
import com.pawelrak.casestudy.view.MainActivity;
import com.pawelrak.casestudy.dagger.module.ActivityModule;
import com.pawelrak.casestudy.view.ProjectDetailsActivity;
import com.pawelrak.data.dagger.ActivityScope;

import dagger.Component;

/**
 * Created by pawelrak on 25.04.2017.
 */

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
    void inject(ProjectDetailsActivity projectDetailsActivity);
    void inject(AddProjectActivity addProjectActivity);

    class Initializer {
        private Initializer() {
        }

        public static ActivityComponent init(ApplicationComponent applicationComponent) {
            return DaggerActivityComponent.builder()
                    .applicationComponent(applicationComponent)
                    .activityModule(new ActivityModule())
                    .build();
        }
    }
}