package com.pawelrak.casestudy.dagger.module;

import android.content.Context;

import com.pawelrak.casestudy.NavigationBus;
import com.pawelrak.casestudy.navigation.Navigator;
import com.pawelrak.casestudy.navigation.handler.ActivityNavigationHandler;
import com.pawelrak.casestudy.navigation.handler.NavigationHandler;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pawelrak on 25.04.2017.
 */

@Singleton
@Module
public class ApplicationModule {
    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    List<NavigationHandler> provideNavigationHandlers() {
        return Arrays.asList(new ActivityNavigationHandler());
    }

    @Singleton
    @Provides
    Navigator provideNavigator(NavigationBus navigationBus,
                               List<NavigationHandler> navigationHandlers) {
        return new Navigator(navigationBus, navigationHandlers);
    }
}
